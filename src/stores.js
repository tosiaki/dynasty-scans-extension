import { writable } from "svelte/store";

export const displayModeStore = writable(
  localStorage.getItem("displayChapters") ?? "list"
);
