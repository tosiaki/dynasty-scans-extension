import HoverThumbnail from "./HoverThumbnail.svelte";
import DisplayToggler from "./DisplayToggler.svelte";
import GridView from "./GridView.svelte";
import { displayModeStore } from "./stores";

HTMLCollection.prototype.find = Array.prototype.find;
HTMLCollection.prototype.map = Array.prototype.map;

window.addEventListener("load", async () => {
  const chapterList = document.getElementsByClassName("chapter-list")[0];
  const chapters = chapterList.getElementsByTagName("dd");
  const chaptersData = Array.prototype.map.call(chapters, (chapter) => {
    const links = chapter.getElementsByTagName("a");
    const chapterLink = links.find((link) => link.href.includes("/chapters/"));
    const doujinLink = links.find((link) => link.href.includes("/doujins/"));
    const authorLink = links.find((link) => link.href.includes("/authors/"));
    const getSrc = (async () => {
      const link = chapterLink.href;
      const parser = new DOMParser();
      const firstPage = parser.parseFromString(
        await (await fetch(link)).text(),
        "text/html"
      );
      const src = firstPage
        .getElementById("image")
        .getElementsByTagName("img")[0].src;
      new HoverThumbnail({
        target: chapter,
        props: {
          src,
        },
      });
      return src;
    })();
    return {
      title: chapterLink.innerText,
      author: authorLink?.innerText,
      tags: Array.prototype.map.call(
        chapter.getElementsByClassName("label"),
        (tagElement) => ({ name: tagElement.innerText, link: tagElement.href })
      ),
      getSrc,
      chapterLink,
      authorLink,
      doujinLink,
    };
  });
  const chapterListParent = chapterList.parentNode;
  new DisplayToggler({
    target: chapterListParent,
    anchor: chapterList,
  });
  new GridView({
    target: chapterListParent,
    anchor: chapterList,
    props: {
      chaptersData,
    },
  });
  displayModeStore.subscribe((value) => {
    if (value === "list") {
      chapterList.style.display = null;
    } else if (value === "grid") {
      chapterList.style.display = "none";
    }
  });
});
